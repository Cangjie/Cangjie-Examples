# 包含完整测试的Monte Carlo积分程序

## 1. 程序结构

本程序是一个完整的Monte Carlo积分程序，包含了所有必要的功能，可以直接运行。

项目的目录结构如下图。

![](imgs/folder-structure.png)

根目录下是我们的示例程序，是一个仓颉工程，包含了调用Monte Carlo积分的主程序`main.cj`，这个程序的依赖在`cjpm.toml`中定义。

```toml
[dependencies]
mci = {path = "mci"}

[package]
  cjc-version = "0.53.5"
  compile-option = ""
  description = "nothing here"
  link-option = ""
  name = "hellomc"
  output-type = "executable"
  src-dir = ""
  target-dir = ""
  version = "1.0.0"
  package-configuration = {}
```

依赖一个名为`mci`的包，这个包是我们的Monte Carlo积分库。

```
|-- hellomc
    |-- mci
        |-- mci.macros
```

`mci`包同样是一个仓颉工程，包含了自己的源代码和依赖的宏定义文件。

## 2. Monte Carlo积分主程序

### 2.1 编译、运行

在对应目录下，可以利用`cjpm`工具编译运行这个程序。

```shell
c:\Cangjie\compiler\envsetup.ps1
cjpm build
cjpm run
```

这里给出的测试函数的实现也很有意思，采用匿名函数的方式定义

$$
f(x) = \sum_{i=1}^{n} x_i^2
$$

```cangjie
{ x => (x |> map {xi => xi * xi} |> reduce {a, b => a + b}).getOrThrow()}
```

这里有管道语法糖`|>`和函数式编程写法，比如`map`和`reduce`函数，还有`Option<T>.getOrThrow`函数。

这个是在Powershell中的运行方式，首先载入仓颉开发的环境，然后编译运行。运行结果如下：这里还记录了程序的运行时间、包括单线程运行和轻量级线程运行。

```
monteCarloIntegral
33.331827
5.858494
20 processors available
newSampleSize: 500000
monteCarloIntegralPara
33.324230
2.336741

cjpm run finished
```

这个程序还可以设定一个参数，也就是采样点的数量，默认是500000个采样点。可以看到，单线程运行时间是5.86秒，20个轻量级线程运行时间是2.34秒。可以看到，全开的轻量级线程运行速度提升并不够明显，这里20个线程，加速比只有2.5左右。

- 程序源文件：[](./src/main.cj)

### 2.2 主程序的测试
可以采用`cjpm test`命令进行测试。

可以看到，测试按照`cjpm tree`给出的依赖关系来进行，首先是`mci`包的测试，然后是`hellomc`包的测试。

```
--------------------------------------------------------------------------------------------------
TP: mci, time elapsed: 3579100 ns, RESULT:
    TCS: TestCase_testReseed, time elapsed: 630900 ns, RESULT:
    [ PASSED ] CASE: testReseed (84400 ns)
    TCS: TestCase_testSample, time elapsed: 108300 ns, RESULT:
    [ PASSED ] CASE: testSample (17500 ns)
    TCS: TestCase_testSampleTuple, time elapsed: 119000 ns, RESULT:
    [ PASSED ] CASE: testSampleTuple (8200 ns)
    TCS: TestCase_testSampleN, time elapsed: 129600 ns, RESULT:
    [ PASSED ] CASE: testSampleN (33000 ns)
    TCS: TestCase_testSampleNTuple, time elapsed: 91200 ns, RESULT:
    [ PASSED ] CASE: testSampleNTuple (10200 ns)
    TCS: TestCase_testSampleArray, time elapsed: 112900 ns, RESULT:
    [ PASSED ] CASE: testSampleArray (14700 ns)
    TCS: TestCase_testVolume, time elapsed: 116900 ns, RESULT:
    [ PASSED ] CASE: testVolume (36100 ns)
    TCS: TestCase_testVolumeN, time elapsed: 93600 ns, RESULT:
    [ PASSED ] CASE: testVolumeN (11100 ns)
    TCS: TestCase_testMonteCarloIntegral, time elapsed: 1067100 ns, RESULT:
    [ FAILED ] CASE: testMonteCarloIntegral (833000 ns)
    Expect Failed: integral != 1.0
     integral: 1.00398
          1.0: 1.0
        delta: 0.00398236

    Assert Failed: `(Got float != Expect float ± Allowed eps by Difference)`
                    |_________|  |____________| |___________|  |__________|
                     1.003982       1.000000      0.001000       0.003982


    TCS: TestCase_testMonteCarloIntegralN, time elapsed: 801400 ns, RESULT:
    [ FAILED ] CASE: testMonteCarloIntegralN (615400 ns)
    Expect Failed: integral != 1.0
     integral: 0.996869
          1.0: 1.0
        delta: -0.00313075

    Assert Failed: `(Got float != Expect float ± Allowed eps by Difference)`
                    |_________|  |____________| |___________|  |__________|
                     0.996869       1.000000      0.001000       0.003131


Summary: TOTAL: 10
    PASSED: 8, SKIPPED: 0, ERROR: 0
    FAILED: 2, listed below:
            TCS: TestCase_testMonteCarloIntegral, CASE: testMonteCarloIntegral
            TCS: TestCase_testMonteCarloIntegralN, CASE: testMonteCarloIntegralN
--------------------------------------------------------------------------------------------------
Project tests finished, time elapsed: 3579100 ns, RESULT:
TP: mci.*, time elapsed: 3579100 ns, RESULT:
    FAILED:
    TP: mci, time elapsed: 3579100 ns, RESULT:
        TCS: TestCase_testReseed, time elapsed: 630900 ns, RESULT:
        TCS: TestCase_testSample, time elapsed: 108300 ns, RESULT:
        TCS: TestCase_testSampleTuple, time elapsed: 119000 ns, RESULT:
        TCS: TestCase_testSampleN, time elapsed: 129600 ns, RESULT:
        TCS: TestCase_testSampleNTuple, time elapsed: 91200 ns, RESULT:
        TCS: TestCase_testSampleArray, time elapsed: 112900 ns, RESULT:
        TCS: TestCase_testVolume, time elapsed: 116900 ns, RESULT:
        TCS: TestCase_testVolumeN, time elapsed: 93600 ns, RESULT:
        TCS: TestCase_testMonteCarloIntegral, time elapsed: 1067100 ns, RESULT:
        [ FAILED ] CASE: testMonteCarloIntegral (833000 ns)
        TCS: TestCase_testMonteCarloIntegralN, time elapsed: 801400 ns, RESULT:
        [ FAILED ] CASE: testMonteCarloIntegralN (615400 ns)
TP: hellomc.*, RESULT:
    EMPTY:
    TP: hellomc
Summary: TOTAL: 10
    PASSED: 8, SKIPPED: 0, ERROR: 0
    FAILED: 2
--------------------------------------------------------------------------------------------------
Error: cjpm test failed
```

这Monte Carlo积分并非是准确的计算，所以测试算例中对于函数的积分值仅能达到一定的精度。所以准确的测试并不能通过。这里，我还实现一个带偏差的浮点数比较函数，可以输出更加详细的比较结果。宏的写法也比较干净，值得注意的时Tokens中间包含了`,`，在获取的时候需要注意跳过。另外，在`quote()`中获取token的值时，需要用`$(input1)`的方式来获取，这里的`$`是反引用。

```cangjie
macro package mci.macros
import std.format.Formatter
// define Macro for testing, Assert float64 equal
import std.ast.*

public macro ExpectFloat64(input: Tokens): Tokens {
    if (input.size != 5) {
        throw Exception("ExpectFloat64: input size must be 5")
    }
    let input1 = input[0]
    let input2 = input[2]
    let eps = input[4]
    let result = quote(
        if (abs($(input1) - $(input2)) > $(eps)) {
            let got = $(input1)
            let ect = $(input2)
            let d = $(eps)
            // let msg = "${got} != ${ect} ± ${d}"
            // failExpect(msg)
            let pad = PowerAssertDiagramBuilder("Got float != Expect float ± Allowed eps by Difference", 0)
            pad.r(got, "Got float", 0)
            pad.r(ect, "Expect float", 13)
            pad.r(d, "Allowed eps", 28)
            pad.r(abs(got - ect), "Difference", 43)
            pad.w(false)
        }
    )
    return result
}
```

这里有一个比较好玩的就是，如果我们用`failExpect`函数登记不符合预期的情况，没办法输出详细的比较结果，只能输出`Expect Failed: integral != 1.0`这样的信息。这里我们用`PowerAssertDiagramBuilder`来输出详细的比较结果，这个类可以记录一个自行定制的详细比较结果，这里包括比较的两个值、允许的误差、实际的误差等等。


## 3. Monte Carlo积分库

这个库到没有什么奇怪的地方，就是产生随机采样，用Monte Carlo方法来逼近积分值。

- 库源文件：[](./src/mci/mci.cj)
- 库测试文件：[](./src/mci/mci_test.cj)
- 库宏定义文件：[](./src/mci/mci.macros)

这里需要注意的是，测试函数所在文件按照`xxx_test.cj`的命名方式，这样`cjpm`工具才能自动识别并运行测试。

## 4. 总结

1. 工程管理比较现代和清晰；
2. 代码风格比较现代，采用函数式编程风格完全没有问题，也没有太多别扭的地方；
3. 匿名函数的调用语法糖也跟Kotlin一样，比较简洁；
4. 轻量级线程用起来很简单，还需要更多分析；
5. 标准库没有源代码简直是……想搞点事情都不行；
6. 宏定义相当简单可用。