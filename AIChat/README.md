### 项目概述
- 使用CangJie编程语言对接OpenAI或者其他兼容OpenAI的接口，完成聊天相关服务。
- 主要测试了[One-API](https://github.com/songquanpeng/one-api)服务提供的接口，以及清华智谱的`glm-4-flash`免费模型，以及dashscope提供的qwen-max，其他openai兼容格式的模型应该问题不大。
- 下面是一个智谱的env.json文件参考：
```json
{
    "model": "glm-4-flash",
    "api_key": "315xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
    "base_url": "https://open.bigmodel.cn/api/paas/v4/chat/completions",
    "system_prompt": "You are a helpful assistant."
}
```
- 下面是一个普通的[One-API](https://github.com/songquanpeng/one-api)服务的env.json文件参考:
```json
{
    "model": "qwen1.5-32b",
    "api_key": "sk-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
    "base_url": "http://192.168.x.x:3000/v1/chat/completions",
    "system_prompt": "You are a helpful assistant."
}
```

- 下面是一个普通的[dashscope]提供的qwen-max服务的env.json文件参考，更多模型可以参考[官方文档](https://help.aliyun.com/zh/dashscope/developer-reference/compatibility-of-openai-with-dashscope)配置。
```json
{
    "model": "qwen-max",
    "api_key": "sk-xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx",
    "base_url": "https://dashscope.aliyuncs.com/compatible-mode/v1/chat/completions",
    "system_prompt": "You are a helpful assistant."
}
```
### 成果展示
![](./image/aichat_cj.gif)

### 使用方法
1. 拷贝一份env_sample.json为env.json，填写对应的信息。

2. 使用cjpm运行即可，默认是非流式对话。
```bash
cjpm run
```

3. 如果需要流式对话，需要加上一个`--stream`参数，如下命令所示。
```bash
cjpm run --run-args "--stream"
```

### 功能一览
- [x] 非流式对话
- [x] 使用SSE协议完成流式对话

### 已知BUG
1. Windows平台下，输入中文对导致程序异常甚至退出。该bug为Cangjie控制台的问题，需要等官方修复，[相关issue](https://gitcode.com/Cangjie/UsersForum/issues/445)
