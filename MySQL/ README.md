> 作者：闲不下来的程序猿

## 项目介绍
示例项目，使用仓颉从MySql数据库查询数据，原生实现，不依赖三方库  
按照 std.database.sql 规范封装  
支持数据类型有限，后续支持更多字段类型
## 运行效果
![](image/查询数据库结果示例.png)
如上，支持创建表、删除表，插入、删除数据
## 项目运行
### 使用docker 安装mysql
在某个目录下，创建data、conf、log 三个文件夹，本文在 /Users/zj/dockerData/mysql 目录下建立上述文件夹  

在conf文件夹下，新建my.cnf 文件，内容如下
```conf
[mysqld]
pid-file        = /var/run/mysqld/mysqld.pid
socket          = /var/run/mysqld/mysqld.sock
datadir         = /var/lib/mysql
lower_case_table_names=1 #实现mysql不区分大小（开发需求，建议开启）
# By default we only accept connections from localhost
bind-address   = 0.0.0.0
# Disabling symbolic-links is recommended to prevent assorted security risks
default-time_zone = '+8:00'

# 更改字符集 如果想Mysql在后续的操作中文不出现乱码,则需要修改配置文件内容
symbolic-links=0
character-set-server=utf8mb4
[client]
default-character-set=utf8mb4
[mysql]
default-character-set=utf8mb4
```

使用如下命令，运行一个docker 容器，其中mysql版本为8.2.0，3个 -v 挂载路径配置成自己的
```sh
docker run -d --restart=always --name mysql \
-v /Users/zj/dockerData/mysql/data:/var/lib/mysql \
-v /Users/zj/dockerData/mysql/conf/my.cnf:/etc/mysql/my.cnf \
-v /Users/zj/dockerData/mysql/log:/var/log/mysql \
-p 3306:3306 \
-e TZ=Asia/Shanghai \
-e MYSQL_ROOT_PASSWORD=12345678 \
mysql:8.2.0 \
--character-set-server=utf8mb4 \
--collation-server=utf8mb4_general_ci
```

进入mysql docker 容器，
```sh
docker exec -it mysql /bin/bash
```
使用root用户连接mysql数据库
```sh
mysql -u root -p
```
输入root 用户密码，给root用户可以登录权限
```sh
use mysql;
alter user 'root'@'localhost' identified with mysql_native_password by '12345678';
flush privileges;
```

### 配置mysql 信息
修改main.cj 第8行  
let connectionString = "root:12345678@tcp(10.200.1.164:3306)/admin-api"  
格式为 用户名:密码@tcp(ip:端口号)/数据库名

### 安装OpenSSH
本项目使用到crypto.digest 包。  
而使用crypto.digest需要外部依赖 OpenSSL 3 的 crypto 动态库文件，故使用前需安装相关工具。  
需要做相关配置，配置见官网