# 四则运算解释器

使用语法解析器组合子(Parser Combinator)的方式实现了一个四则运算解释器

## 用法

```sh
-> cd SimpleMathInterpreter
-> cjpm run
###################################################
##  Enter an simple math expression.             ##
##  e.g.                                         ##
##  3 + 2 * (99 + 101) + 7                       ##
##  1+(1 -1) + 6* 30/9                           ##
##                                               ##
##  Press Ctrl + C to exit                       ##
###################################################

3 + 2 * (99 + 101) + 7
410
1+7
8
(7
Invalid Expression!
1+(1 -1) + 6* 30/9      
21
```

## TODO

1. 注释
2. 词法定义和语法定义
3. 单元测试

## 参考

1. [parsec: Monadic parser combinators](https://hackage.haskell.org/package/parsec)
2. [Learning Parser Combinators With Rust](https://bodil.lol/parser-combinators/)