# Macro_JsonSerializable

示例项目^_^

### 示例介绍
    使用仓颉语言的自定义宏能力，实现为结构体实现 Serializable 接口，如果声明要实现 ToJson 或者 ToString 接口，宏也会为你生成对应的方法。

### 示例
```cangjie
package Macro_JsonSerializable

import serialization.serialization.*
import encoding.json.*
import Macro_JsonSerializable.macros.*

@JsonSerializable[private]
public struct S0 <: Serializable<S0> & ToString & ToJson {
    let name: String
}

@JsonSerializable[protected]
public struct S1<T> <: Serializable<S1<T>> & ToString & ToJson where T <: Serializable<T> {
    let name: String
    let t: T
}

@JsonSerializable[internal]
public struct S2<T, Q> <: Serializable<S2<T, Q>> & ToString & ToJson where T <: Serializable<T>, Q <: Serializable<Q> {
    let t: T
    let q: Q
}

@JsonSerializable[public]
public struct S3<T, Q> <: Serializable<S3<T, Q>> & ToString & ToJson where T <: Serializable<T>, Q <: Serializable<Q> {
    let t: Option<T>
    let q: ????Q
}

main() {
    println(S3(t: "T", q: "Q")) // {"t":"T","q":"Q"}
}
```

生成的代码如下

```cangjie
package Macro_JsonSerializable

import serialization.serialization.*
import encoding.json.*
import Macro_JsonSerializable.macros.*

/* ===== Emitted by MacroCall @JsonSerializable in main.cj:7:1 ===== */
/* 7.1 */public struct S0 <: Serializable < S0 > & ToString & ToJson {
/* 7.2 */    let name: String
/* 7.3 */    private init(name!: String) {
/* 7.4 */        this.name = name
/* 7.5 */    }
/* 7.6 */    static public func deserialize(dm: DataModel): S0 {
/* 7.7 */        let dms = match(dm) {
/* 7.8 */            case d: DataModelStruct => d
/* 7.9 */            case _ => throw Exception("This data is not DataModelStruct")
/* 7.10 */        }
/* 7.11 */        return S0(name: String.deserialize(dms.get("name")))
/* 7.12 */    }
/* 7.13 */    public func serialize(): DataModel {
/* 7.14 */        let dms = DataModelStruct()
/* 7.15 */        dms.add(field("name", this.name))
/* 7.16 */        return dms
/* 7.17 */    }
/* 7.18 */    public func toString(): String {
/* 7.19 */        return this.serialize().toJson().toString()
/* 7.20 */    }
/* 7.21 */    public func toJsonString(): String {
/* 7.22 */        return this.serialize().toJson().toJsonString()
/* 7.23 */    }
/* 7.24 */    static public func fromJson(jv: JsonValue): DataModel {
/* 7.25 */        return DataModel.fromJson(jv)
/* 7.26 */    }
/* 7.27 */    public func toJson(): JsonValue {
/* 7.28 */        return this.serialize().toJson()
/* 7.29 */    }
/* 7.30 */}
/* 7.31 */
/* ===== End of the Emit ===== */

/* ===== Emitted by MacroCall @JsonSerializable in main.cj:12:1 ===== */
/* 12.1 */public struct S1 < T > <: Serializable < S1 < T >> & ToString & ToJson where T <: Serializable < T > {
/* 12.2 */    let name: String
/* 12.3 */    let t: T
/* 12.4 */    protected init(name!: String, t!: T) {
/* 12.5 */        this.name = name
/* 12.6 */        this.t = t
/* 12.7 */    }
/* 12.8 */    static public func deserialize(dm: DataModel): S1 < T > {
/* 12.9 */        let dms = match(dm) {
/* 12.10 */            case d: DataModelStruct => d
/* 12.11 */            case _ => throw Exception("This data is not DataModelStruct")
/* 12.12 */        }
/* 12.13 */        return S1 < T >(name: String.deserialize(dms.get("name")), t: T.deserialize(dms.get("t")))
/* 12.14 */    }
/* 12.15 */    public func serialize(): DataModel {
/* 12.16 */        let dms = DataModelStruct()
/* 12.17 */        dms.add(field("name", this.name))
/* 12.18 */        dms.add(field("t", this.t))
/* 12.19 */        return dms
/* 12.20 */    }
/* 12.21 */    public func toString(): String {
/* 12.22 */        return this.serialize().toJson().toString()
/* 12.23 */    }
/* 12.24 */    public func toJsonString(): String {
/* 12.25 */        return this.serialize().toJson().toJsonString()
/* 12.26 */    }
/* 12.27 */    static public func fromJson(jv: JsonValue): DataModel {
/* 12.28 */        return DataModel.fromJson(jv)
/* 12.29 */    }
/* 12.30 */    public func toJson(): JsonValue {
/* 12.31 */        return this.serialize().toJson()
/* 12.32 */    }
/* 12.33 */}
/* 12.34 */
/* ===== End of the Emit ===== */

/* ===== Emitted by MacroCall @JsonSerializable in main.cj:18:1 ===== */
/* 18.1 */public struct S2 < T, Q > <: Serializable < S2 < T, Q >> & ToString & ToJson where T <: Serializable < T >, Q <: Serializable < Q > {
/* 18.2 */    let t: T
/* 18.3 */    let q: Q
/* 18.4 */    internal init(t!: T, q!: Q) {
/* 18.5 */        this.t = t
/* 18.6 */        this.q = q
/* 18.7 */    }
/* 18.8 */    static public func deserialize(dm: DataModel): S2 < T, Q > {
/* 18.9 */        let dms = match(dm) {
/* 18.10 */            case d: DataModelStruct => d
/* 18.11 */            case _ => throw Exception("This data is not DataModelStruct")
/* 18.12 */        }
/* 18.13 */        return S2 < T, Q >(t: T.deserialize(dms.get("t")), q: Q.deserialize(dms.get("q")))
/* 18.14 */    }
/* 18.15 */    public func serialize(): DataModel {
/* 18.16 */        let dms = DataModelStruct()
/* 18.17 */        dms.add(field("t", this.t))
/* 18.18 */        dms.add(field("q", this.q))
/* 18.19 */        return dms
/* 18.20 */    }
/* 18.21 */    public func toString(): String {
/* 18.22 */        return this.serialize().toJson().toString()
/* 18.23 */    }
/* 18.24 */    public func toJsonString(): String {
/* 18.25 */        return this.serialize().toJson().toJsonString()
/* 18.26 */    }
/* 18.27 */    static public func fromJson(jv: JsonValue): DataModel {
/* 18.28 */        return DataModel.fromJson(jv)
/* 18.29 */    }
/* 18.30 */    public func toJson(): JsonValue {
/* 18.31 */        return this.serialize().toJson()
/* 18.32 */    }
/* 18.33 */}
/* 18.34 */
/* ===== End of the Emit ===== */

/* ===== Emitted by MacroCall @JsonSerializable in main.cj:24:1 ===== */
/* 24.1 */public struct S3 < T, Q > <: Serializable < S3 < T, Q >> & ToString & ToJson where T <: Serializable < T >, Q <: Serializable < Q > {
/* 24.2 */    let t: Option < T >
/* 24.3 */    let q: Option < Option < Option < Option < Q >>>>
/* 24.4 */    public init(t!: Option < T >, q!: Option < Option < Option < Option < Q >>>>) {
/* 24.5 */        this.t = t
/* 24.6 */        this.q = q
/* 24.7 */    }
/* 24.8 */    static public func deserialize(dm: DataModel): S3 < T, Q > {
/* 24.9 */        let dms = match(dm) {
/* 24.10 */            case d: DataModelStruct => d
/* 24.11 */            case _ => throw Exception("This data is not DataModelStruct")
/* 24.12 */        }
/* 24.13 */        return S3 < T, Q >(t: Option < T >.deserialize(dms.get("t")), q: Option < Option < Option < Option < Q >>>>.deserialize(dms.get("q")))
/* 24.14 */    }
/* 24.15 */    public func serialize(): DataModel {
/* 24.16 */        let dms = DataModelStruct()
/* 24.17 */        dms.add(field("t", this.t))
/* 24.18 */        dms.add(field("q", this.q))
/* 24.19 */        return dms
/* 24.20 */    }
/* 24.21 */    public func toString(): String {
/* 24.22 */        return this.serialize().toJson().toString()
/* 24.23 */    }
/* 24.24 */    public func toJsonString(): String {
/* 24.25 */        return this.serialize().toJson().toJsonString()
/* 24.26 */    }
/* 24.27 */    static public func fromJson(jv: JsonValue): DataModel {
/* 24.28 */        return DataModel.fromJson(jv)
/* 24.29 */    }
/* 24.30 */    public func toJson(): JsonValue {
/* 24.31 */        return this.serialize().toJson()
/* 24.32 */    }
/* 24.33 */}
/* 24.34 */
/* ===== End of the Emit ===== */

main() {
    println(S3(t: "T", q: "Q")) // {"t":"T","q":"Q"}
}
```