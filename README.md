# Cangjie-Examples

本仓将收集和展示高质量的仓颉示例代码（仓颉鸿蒙应用示例请移步 [HarmonyOS-Examples](https://gitcode.com/Cangjie/HarmonyOS-Examples) 仓），欢迎大家投稿，让全世界看到您的妙趣设计，也让更多人通过您的编码理解和喜爱仓颉语言。

本仓收录的仓颉项目，侧重于仓颉教学和宣传，示例代码需要在趣味性、启发性、艺术性、原理揭示、文化传播等方面有适当设计，同时示例代码要符合仓颉编码规范。

